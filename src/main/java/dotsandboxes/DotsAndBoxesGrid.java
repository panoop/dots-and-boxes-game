package dotsandboxes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Consumer;

public class DotsAndBoxesGrid {

    final int width;
    final int height;

    private boolean[][] horizontals;
    private boolean[][] verticals;
    private int[][] boxOwners;

    private ArrayList<Consumer<DotsAndBoxesGrid>> watchers = new ArrayList<>();
    final int players;
    private int player = 1;

    public int getPlayer() {
        return player;
    }

    private void nextPlayer() {
        player++;
        if (player > players) {
            player = 1;
        }
    }

    public DotsAndBoxesGrid(int width, int height, int players) {
        this.width = width;
        this.height = height;
        this.players = players;

        this.horizontals = new boolean[width - 1][height];
        this.verticals = new boolean[width][height - 1];
        this.boxOwners = new int[width - 1][height - 1];
    }

    private void notifyObservers() {
        for (Consumer<DotsAndBoxesGrid> consumer : watchers) {
            consumer.accept(this);
        }
    }

    public void addConsumer(Consumer<DotsAndBoxesGrid> consumer) {
        watchers.add(consumer);
    }

    public boolean getHorizontal(int x, int y) {
        return horizontals[x][y];
    }

    public boolean getVertical(int x, int y) {
        return verticals[x][y];
    }

    public int getBoxOwner(int x, int y) {
        return boxOwners[x][y];
    }

    public boolean boxComplete(int x, int y) {
        if (x >= width - 1 || x < 0 || y >= height - 1 || y < 0) {
            return false;
        }

        return horizontals[x][y] && horizontals[x][y+1] && verticals[x][y] && verticals[x+1][y];
    }

    private boolean claimBox(int x, int y, int p) {
        if (boxComplete(x, y)) {
            boxOwners[x][y] = player;
            return true;
        } else {
            return false;
        }
    }

    public boolean drawHorizontal(int x, int y, int player) {
        if (x >= width - 1 || x < 0) {
            throw new IndexOutOfBoundsException(String.format("x was %d, which is out of range. Range is 0 to %d", x, width - 1));
        }
        if (y >= height || y < 0) {
            throw new IndexOutOfBoundsException(String.format("y was %d, which is out of range. Range is 0 to %d", y, height));
        }

        if (horizontals[x][y]) {
            throw new IllegalStateException("This line has already been drawn.");
        }

        this.horizontals[x][y] = true;

        boolean claimN = claimBox(x, y-1, player);
        boolean claimS = claimBox(x, y, player);
        if (claimN || claimS) {
            notifyObservers();
            return true;
        } else {
            nextPlayer();
            notifyObservers();
            return false;
        }
    }

    public boolean drawVertical(int x, int y, int player) {
        if (x >= width || x < 0) {
            throw new IndexOutOfBoundsException(String.format("x was %d, which is out of range. Range is 0 to %d", x, width));
        }
        if (y >= height - 1 || y < 0) {
            throw new IndexOutOfBoundsException(String.format("y was %d, which is out of range. Range is 0 to %d", y, height - 1));
        }

        if (verticals[x][y]) {
            throw new IllegalStateException("This line has already been drawn.");
        }

        this.verticals[x][y] = true;

        boolean claimE = claimBox(x, y, player);
        boolean claimW = claimBox(x-1, y, player);
        if (claimE || claimW) {
            notifyObservers();
            return true;
        } else {
            nextPlayer();
            notifyObservers();
            return false;
        }
    }

    public boolean gameComplete() {
        return Arrays.stream(boxOwners).allMatch((row) -> Arrays.stream(row).allMatch((owner) -> owner > 0));
    }
}

